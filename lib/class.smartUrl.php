<?php

namespace smartUrl;

class smartUrl {

    private $method = 'get';
    private $handling;
    public $parameter;
    private $adminMode = FALSE;
    private $stringParameters;
    private $languages ;
    private $defilterArray;

    private $routeConfig;

    private $firstElementAppVersion;
    private $appVersion;

    function __construct() {
        $this->setUrl();
    }

    //set the proper language
    public function setLanguages($languagesArray)
    {
        $this->languagesArray = $languagesArray;
    }

    //filter language from the url
    public function setUrl(){
        // if there arey not allowed url structure, this wil be filtered out the url
        $requestRoot = [];
        $this->languagesArray = array('nl','en');
        $this->defilterArray = array_merge( $this->languagesArray,$requestRoot);
        $this->firstElementAppVersion = true;

        // set the requested method
        $this->setMethod() ;
        // set the requested url
        $this->setArrayParameter();
    }

    // get the requested method
    public function getMethod()
    {
        return $this->method;
    }

    // set the requested method
    private function setMethod(){
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
    }

    // convert string to normal characters for a url
    private function getConvertUrlArr(){
        return array('à' => 'a',' ' => '_',  'ô' => 'o',  '�' => 'd',  'ḟ' => 'f',  'ë' => 'e',  'š' => 's',  'ơ' => 'o', 'ß' => 'ss', 'ă' => 'a',  'ř' => 'r',  'ț' => 't',  'ň' => 'n',  '�' => 'a',  'ķ' => 'k','�' => 's',  'ỳ' => 'y',  'ņ' => 'n',  'ĺ' => 'l',  'ħ' => 'h',  'ṗ' => 'p',  'ó' => 'o','ú' => 'u',  'ě' => 'e',  'é' => 'e',  'ç' => 'c',  '�' => 'w',  'ċ' => 'c',  'õ' => 'o','ṡ' => 's',  'ø' => 'o',  'ģ' => 'g',  'ŧ' => 't',  'ś' => 's',  'ė' => 'e',  'ĉ' => 'c','ś' => 's',  'î' => 'i',  'ű' => 'u',  'ć' => 'c',  'ę' => 'e',  'ŵ' => 'w',  'ṫ' => 't','ū' => 'u',  '�' => 'c',  'ö' => 'o',  'è' => 'e',  'ŷ' => 'y',  'ą' => 'a',  'ł' => 'l','ų' => 'u',  'ů' => 'u',  'ş' => 's',  'ğ' => 'g',  'ļ' => 'l',  'ƒ' => 'f',  'ž' => 'z','ẃ' => 'w',  'ḃ' => 'b',  'å' => 'a',  'ì' => 'i',  'ï' => 'i',  'ḋ' => 'd',  'ť' => 't','ŗ' => 'r',  'ä' => 'a',  'í' => 'i',  'ŕ' => 'r',  'ê' => 'e',  'ü' => 'u',  'ò' => 'o','ē' => 'e',  'ñ' => 'n',  'ń' => 'n',  'ĥ' => 'h',  '�' => 'g',  'đ' => 'd',  'ĵ' => 'j','ÿ' => 'y',  'ũ' => 'u',  'ŭ' => 'u',  'ư' => 'u',  'ţ' => 't',  'ý' => 'y',  'ő' => 'o','â' => 'a',  'ľ' => 'l',  'ẅ' => 'w',  'ż' => 'z',  'ī' => 'i',  'ã' => 'a',  'ġ' => 'g','�' => 'm',  '�' => 'o',  'ĩ' => 'i',  'ù' => 'u',  'į' => 'i',  'ź' => 'z',  'á' => 'a','û' => 'u',  'þ' => 'th', 'ð' => 'dh', 'æ' => 'ae', 'µ' => 'u',  'ĕ' => 'e','À' => 'A',  'Ô' => 'O',  'Ď' => 'D',  'Ḟ' => 'F',  'Ë' => 'E',  'Š' => 'S',  'Ơ' => 'O','Ă' => 'A',  'Ř' => 'R',  'Ț' => 'T',  'Ň' => 'N',  'Ā' => 'A',  'Ķ' => 'K',  'Ĕ' => 'E','Ŝ' => 'S',  'Ỳ' => 'Y',  'Ņ' => 'N',  'Ĺ' => 'L',  'Ħ' => 'H',  'Ṗ' => 'P',  'Ó' => 'O','Ú' => 'U',  'Ě' => 'E',  'É' => 'E',  'Ç' => 'C',  'Ẁ' => 'W',  'Ċ' => 'C',  'Õ' => 'O','Ṡ' => 'S',  'Ø' => 'O',  'Ģ' => 'G',  'Ŧ' => 'T',  'Ș' => 'S',  'Ė' => 'E',  'Ĉ' => 'C','Ś' => 'S',  'Î' => 'I',  'Ű' => 'U',  'Ć' => 'C',  'Ę' => 'E',  'Ŵ' => 'W',  'Ṫ' => 'T','Ū' => 'U',  'Č' => 'C',  'Ö' => 'O',  'È' => 'E',  'Ŷ' => 'Y',  'Ą' => 'A',  '�' => 'L','Ų' => 'U',  'Ů' => 'U',  'Ş' => 'S',  'Ğ' => 'G',  'Ļ' => 'L',  'Ƒ' => 'F',  'Ž' => 'Z','Ẃ' => 'W',  'Ḃ' => 'B',  'Å' => 'A',  'Ì' => 'I',  '�' => 'I',  'Ḋ' => 'D',  'Ť' => 'T','Ŗ' => 'R',  'Ä' => 'A',  '�' => 'I',  'Ŕ' => 'R',  'Ê' => 'E',  'Ü' => 'U',  'Ò' => 'O','Ē' => 'E',  'Ñ' => 'N',  'Ń' => 'N',  'Ĥ' => 'H',  'Ĝ' => 'G',  '�' => 'D',  'Ĵ' => 'J','Ÿ' => 'Y',  'Ũ' => 'U',  'Ŭ' => 'U',  'Ư' => 'U',  'Ţ' => 'T',  '�' => 'Y',  '�' => 'O','Â' => 'A',  'Ľ' => 'L',  'Ẅ' => 'W',  'Ż' => 'Z',  'Ī' => 'I',  'Ã' => 'A',  'Ġ' => 'G','Ṁ' => 'M',  'Ō' => 'O',  'Ĩ' => 'I',  'Ù' => 'U',  'Į' => 'I',  'Ź' => 'Z',  '�' => 'A','Û' => 'U',  'Þ' => 'Th', '�' => 'Dh', 'Æ' => 'Ae', ' ' => '_', '%20' => '_', ',' => '', '\'' => '', ';' => '', ':' => '',  '<' => '', '>' => '',  '\\' => '', '{' => '', '}' => '', '[' => '', ']' => '', '|' => '', '+' => '',  '!' => '', '@' => '', '#' => '', '$' => '', '%' => '', '^' => '', '&' => '', '*' => '', '(' => '', ')' => '', '"' => '', '`' => '', '~' => '', '^' => '', );
    }

    private function getConvertHumanArr(){
        return '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ !@#$%^&*()-=_+{}|[]\\:";\'<>?,./`~/*-+';
    }

    // convert a string to normal characters for a url
    public function friendlyUrl($url){
        $url = strtolower($url);
        $convert_table = $this->getConvertUrlArr();
        return str_replace(array_keys($convert_table), array_values($convert_table), $url );
    }

    // convert a string to friendly human characters
    public function friendlyHuman($str){
        $arr = str_split($str);
        $allowedChar = $this->getConvertHumanArr();
        $strReturn = '';
        foreach ($arr as $char) {
            if(strpos($allowedChar,$char ) !== false){
                $strReturn .= $char;
            }
        }
        return $strReturn;
    }

    // set the request url in a paramater array
    private function setArrayParameter(){
        $convert_table = $this->getConvertUrlArr();
        // make sure there are no allowed characters inslide the url
        $replaceREQUEST_URI = str_replace(array_keys($convert_table), array_values($convert_table), $_SERVER['REQUEST_URI'] );
        $replaceREQUEST_URI = substr($replaceREQUEST_URI, 1);

        // get the get request out of the url paramaters
        $filterQuestionMark = explode('?',$replaceREQUEST_URI);
        $requestURL = explode('/',$filterQuestionMark[0]);

        $maxCount =  count($requestURL);
        for($i= 0;$i < $maxCount ;$i++){
            // filter out all not allowed parameters like languages array
            if( in_array($requestURL[$i] ,$this->defilterArray)  ){

                // set the found page languages on languages var
                if( in_array($requestURL[$i], $this->languagesArray)  ){
                    $this->languages = $requestURL[$i];
                }
                unset($requestURL[$i]);
            }
        }
        $requestURL = array_values($requestURL);

        // the firt element is the always the version nummer
        if($this->firstElementAppVersion)
        {
            $this->appVersion =  $requestURL[0];
            unset($requestURL[0]);
        }

        // make a clien array for all the allowed paramaters
        $this->parameter = array_values($requestURL);
    }



    public function getParameter($number = false)
    {
        if($number === false)
        {
            return $this->parameter;
        }else
        {
            if(!isset($this->parameter[$number]))
            {
                trigger_error("fatal#route201invalid number in the paramater",E_USER_ERROR);
            }
            return $this->parameter[$number];
        }
    }

    public function getLanguages()
    {
        return ($this->languages ? $this->languages : FALSE);
    }



    public function getAppVersion()
    {
        if($this->appVersion){
            return $this->appVersion;
        }

        $parm = $this->getParameter();
        return $parm[0];
    }

};
